#toy module to implement fibonacci sequence in construction of music
#selects notes based on membership of major/minor scales
#also modifies tempo based on an arbitrary algorithm which uses
#fibonacci sequence as input

require 'midilib/sequence'
require 'midilib/consts'
include MIDI

seq = Sequence.new()

# Create a first track for the sequence. This holds tempo events and stuff
# like that.
track = Track.new(seq)
seq.tracks << track
track.events << Tempo.new(Tempo.bpm_to_mpq(120))
track.events << MetaEvent.new(META_SEQ_NAME, 'Sequence Name')

track = Track.new(seq)
seq.tracks << track


@cache = {}; @cache[1]=1; @cache[2]=1
	def fibonacci(n)
		@cache[n] ||=fibonacci(n-1) + fibonacci(n-2)
	end


def fib_upper_ratio(n)

	out = 1.0*fibonacci(n+1)/fibonacci(n)
	return out
end

def fib_lower_ratio(n)

	out = 1.0*fibonacci(n)/fibonacci(n+1)
	return out
end

class Float
  def to_nearest_i
    (self+0.5).to_i
  end
end


#notes

MIDI_MAX=127
VELOCITY_MAX=127
NUMBER_OF_NOTES=1000
BASELINE_TEMPO=120
OCTAVE_INTERVAL=12

# Give the track a name and an instrument name (optional).
track.name = 'Fibonacci Scale'
track.instrument = GM_PATCH_NAMES[0]

# Add a volume controller event (optional).
track.events << Controller.new(0, CC_VOLUME, 127)

track.events << ProgramChange.new(0, 1, 0)
quarter_note_length = seq.note_to_delta('quarter')


#scales used for filtering

major_notes = [0,2,4,5,7,9,11,12]
minor_notes = [0,2,3,5,7,8,10,12]

current_val=0
counter=1
new_tempo=seq.tempo
loop do
	counter+=1
	current_val=fibonacci(counter).modulo(MIDI_MAX)
	minor_filter=current_val.modulo(OCTAVE_INTERVAL)
	if counter==NUMBER_OF_NOTES
		break
	end
	if minor_notes.include? minor_filter
		puts "for instance #{counter}, fibonacci number is #{current_val}"

	  note_length = seq.length_to_delta(1.0*fibonacci(counter)/fibonacci(counter+1))

	  track.events << NoteOn.new(0, 0 + current_val, VELOCITY_MAX, 0)
	  track.events << NoteOff.new(0, 0 + current_val, VELOCITY_MAX, note_length)		

	  puts "fib upper ratio is #{fib_upper_ratio(counter)}"
	  puts "fib lower ratio is #{fib_lower_ratio(counter)}"
	  fib_diff=(1.0*fib_lower_ratio(counter)-0.618).abs
	  puts "fib_diff is #{fib_diff}"

	  new_tempo = new_tempo * (1 + (1.0*fib_lower_ratio(counter)-0.618).abs)

	  new_tempo = new_tempo.to_nearest_i

	  puts "for instance #{new_tempo} is new tempo"
	  track.events << MIDI::Tempo.new(MIDI::Tempo.bpm_to_mpq(new_tempo))
	end

end


File.open('from_scratch_minor.mid', 'wb') { |file| seq.write(file) }


