require 'benchmark' #Ruby module
require 'midilib/sequence'
require 'midilib/io/seqwriter'


seq = MIDI::Sequence.new()

# Read the contents of a MIDI file into the sequence.
File.open('Back_In_Black.mid', 'rb') { | file |
    seq.read(file) { | track, num_tracks, i |
        # Print something when each track is read.
        puts "read track #{i} of #{num_tracks}"
    }
}


# Iterate over every event in every track.
seq.each { | track |
    track.each { | event |
        # If the event is a note event (note on, note off, or poly
        # pressure) and it is on MIDI channel 5 (channels start at
        # 0, so we use 4), then transpose the event down one octave.

    	puts "read track #{track} of #{event}"

    }
}



# Start with a sequence that has something worth saving.
#seq = read_or_create_seq_we_care_not_how()

# Write the sequence to a MIDI file.
File.open('my_output_file.mid', 'wb') { | file | seq.write(file) }





time = Benchmark.realtime do

@cache = {}; @cache[1]=1; @cache[2]=1

	n = 49
	def fibonacci(n)
		@cache[n] ||=fibonacci(n-1) + fibonacci(n-2)

	end

puts "#{n}'s fibonacci value is #{fibonacci(n)}"

end #close benchmark block

puts "Time elapsed #{time} seconds"
